#Intro to Semantic Web at Hofstra University
#Final Project

A comparison of methods of semantic algorithm, using tf-idf and NER.

Components:
Web Crawler: Crawls news articles to create a corpus of documents to parse.
NER Tagging: Uses StanfordNlp to tag entities on a given document.
TF-IDF: Calculates the tf-idf score of words throughout the corpus, returning the terms with the highest scores.
Parser: Evaluates pages gathered by the web crawler and eliminates unnecessary tags and elements.