package com.semweb.modules.main;

import java.util.*;
import com.semweb.modules.*;
import com.semweb.modules.tfidf.TermFrequency;
import com.semweb.util.*;

public class Debug {

  public static void main(String[] args) {
      DataHandler dHandler = new DataHandler("localhost", 27017, "test");
      List<List<String>> corpus = dHandler.constructCorpus(8);

      // //Print out corpus
      // System.out.println("Size of corpus: " + corpus.size());
      // for (List<String> doc : corpus) {
      //   System.out.println("**DOCUMENT START**");
      //   for (String s : doc) {
      //       System.out.print(s);
      //   }
      //   System.out.println("\n**DOCUMENT END** \n");
      // }
      //
      // //corpus tfidf
      // TermFrequency termFreq = new TermFrequency(corpus);
      // double score = termFreq.getScore("Google");
      // System.out.println("TFIDF score is: " + score);
  }

}
