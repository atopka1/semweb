package com.semweb.modules.tfidf;

import java.util.*;

public class TermFrequency {

  public static final int MAX_TERMS_TO_RETURN = 10;
  private List<List<String>> corpus;
  private List<String> startDocument;

  //constructor
  public TermFrequency(List<List<String>> corpus) {
      this.corpus = corpus;

      if (corpus.size() > 0) {
          this.startDocument = corpus.get(0); //Default to first doc in corpus. TODO: Specify?
      }
      else {
          System.out.println("\n**Error** Corpus is empty.\n");
      }
  }

  //return list of terms
  public List<String> getTopTerms() {
      List<String> terms = new ArrayList<String>();
      return terms;
  }

  //# of times term occurs in document
  private double tf(String term, List<String> document) {
      double freq = 0;
      for (String word : document) {
          if (term.equalsIgnoreCase(word)) {
            freq++;
          }
      }

      System.out.println("Number of occurences: " + freq);
      return freq / document.size();
  }

  //log of (total # of docs / # of docs containing term)
  private double idf(String term) {
      double n = 0;
      for (List<String> doc : corpus) {
          for (String word : doc) {
              if (term.equalsIgnoreCase(word)) {
                n++;
                break; // I hate breaks
              }
          }
      }
      return Math.log(corpus.size() / n);
  }

  //score of individual term = tf * idf
  public double getScore(String term) {
      return tf(term, startDocument) * idf(term);
  }
}
