package com.semweb.util;

import java.util.*;
import com.mongodb.*;
import com.semweb.util.Parser;

public class DataHandler {
  private String servername;
  private int portnumber;
  private String dbname;
  private DB db;

  //constructor
  public DataHandler(String servername, int portnumber, String dbname) {
      this.servername = servername;
      this.portnumber = portnumber;
      this.dbname = dbname;

      this.connect();
  }

  private void connect() {
      try {
          MongoClient mongoClient = new MongoClient(servername, portnumber);

          db = mongoClient.getDB(dbname);
          System.out.println("\n**Success** Connected to MongoDB\n");

      }
      catch (Exception e) {
        System.err.println(e.getClass().getName() + ": " + e.getMessage());
      }
  }

  public void insertUrl(String url) {
      try {
          DBCollection coll = db.getCollection("articles"); //might have to create this first using db.createCollection("articles")

          BasicDBObject doc = new BasicDBObject("url", url);

          coll.insert(doc);
          System.out.println("Document inserted");
      }
      catch (Exception e)  {
          System.err.println(e.getClass().getName() + ": " + e.getMessage());
      }
  } //insertUrl();

  public List<String> getUrls(int limit) {
      List<String> urls = new ArrayList<String>();
      try {
          DBCollection coll = db.getCollection("articles");
          BasicDBObject query = new BasicDBObject();
          BasicDBObject field = new BasicDBObject();
          field.put("url", 1);

          DBCursor cursor = coll.find(query, field);
          int i = 1;

          while (cursor.hasNext() && i <= limit) {
              BasicDBObject obj = (BasicDBObject) cursor.next();
              urls.add(obj.getString("url"));
              i++;
          }
      }
      catch (Exception e) {
          System.out.println("**Error** Can't retrive urls from Mongo");
          System.err.println(e.getClass().getName() + ": " + e.getMessage());
      }
      return urls;
  }

  //get top *limit* urls in database and  construct a list with document bodies
  public List<List<String>> constructCorpus(int limit) {
      List<String> urls = getUrls(limit);
      List<List<String>> corpus = new ArrayList<List<String>>();
      Parser parser = new Parser();

      for (String url : urls) {
          String body = parser.getDocumentBody(url);
          List<String> doc = new ArrayList<String>();

          doc = Arrays.asList(body.split("(?!^)"));
          corpus.add(doc);
      }

      return corpus;
  }

}
