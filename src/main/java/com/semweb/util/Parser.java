package com.semweb.util;

import com.semweb.util.DataHandler;

import java.util.*;
import java.net.*;
import java.io.*;

import org.jsoup.Jsoup;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Parser {

    //Grab body content from HTML page
    public String getDocumentBody(String url) {
        String result = "";

        try {
            Connection connection = Jsoup.connect(url);
            Document htmlDocument = connection.get();

            //validate connection
            if (connection.response().statusCode() == 200) {
              System.out.println("Web page found at " + url);
            }
            if (!connection.response().contentType().contains("text/html")) {
              System.out.println("**Error** Retrieved something other than html");
            }

            //get document body
            Elements paragraphs = htmlDocument.select("p");
            for (Element p : paragraphs) {
              result += p.text();
            }
        }
        catch (IOException e) {
          e.printStackTrace();
          System.out.println("**Error** Could not fetch from url");
        }
        return result;
    }
}
