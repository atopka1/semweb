/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nlp;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.*;
import java.util.*;
import edu.stanford.nlp.pipeline.*;
import java.util.regex.PatternSyntaxException;
/**
 *
 * @author Benji
 */
public class Nlp {
    
    String[] websites = {};
    
    //when you instantiate Nlp class, give the constructor the array of urls
    public Nlp(String[] urls){
        this.websites = urls;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        String[] url = {
            "http://www.marketwatch.com/story/netflix-earnings-as-the-streaming-company-increases-content-investment-heres-what-to-expect-2017-04-12",
            "http://www.investopedia.com/articles/investing/060815/how-netflix-changing-tv-industry.asp"
        };
        
        Nlp example = new Nlp(url);
        example.runNlpTagger();
        
        
    }
    
    public  void runNlpTagger(){
        try{
          //Part 1 - NER - tag each document passed to jsoupParse function.
          
          //first, tag each document within the String[] 
          for (String s : this.websites){
            
            //get a title from the url 
            String title = getTitle(s);
             
            String fin = jsoupParse(s);
            NERDemo ner = new NERDemo();
            ner.NERprint(fin, title);  
          }//go through document archive and tag each document
          
          
        } catch (IOException e) {
            e.printStackTrace();
        }
    }   
    
    public String getTitle(String url){
         //get a title from the article
        String title = "";
        String[] spl = url.split("/");
        title = spl[spl.length-1];
        return title;
    }
    
    public String jsoupParse(String url){
       
        
        //now parse the article for data and return the raw text as a string
        
         try {
            String fin = "";
            Document doc = Jsoup.connect(url).get();
            Elements paragraphs = doc.select("p");
            
            for(Element p : paragraphs) {
              //System.out.println(p.text());
              fin += p.text();
            }
            
            System.out.println("\n");
            return fin;
            
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "fatal error";
        }  
    }

}//NLP Class

class NERDemo {

  public void NERprint(String s, String title) throws IOException {
    String niceOutput = title + ".txt";
    String rawOutput = "raw " + title + ".txt";
    PrintWriter out = new PrintWriter(niceOutput);
    PrintWriter rawOut = new PrintWriter(rawOutput);
    
    // Create a CoreNLP pipeline. To build the default pipeline, you can just use:
    //   StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
    // Here's a more complex setup example:
    //   Properties props = new Properties();
    //   props.put("annotators", "tokenize, ssplit, pos, lemma, ner, depparse");
    //   props.put("ner.model", "edu/stanford/nlp/models/ner/english.all.3class.distsim.crf.ser.gz");
    //   props.put("ner.applyNumericClassifiers", "false");
    //   StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

    // Add in sentiment
    Properties props = new Properties();
    // parse, dcoref, sentiment
    props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner");

    StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

    // Initialize an Annotation with some text to be annotated. The text is the argument to the constructor.
    Annotation annotation = new Annotation(s);
    

    // run all the selected Annotators on this text
    pipeline.annotate(annotation);
     // this prints out the results of sentence analysis to file(s) in good formats
   
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    
    pipeline.prettyPrint(annotation, baos);
    
    String stringOutput = baos.toString();
    
    //raw output to text file
    rawOut.println(stringOutput);
    
    //clean the stringOutput so the text file only shows
    //the Name tag and the EntityRecognition tag
    //
    //examlple
    //      
    //  [Text=Netflix
    //      NamedEntityTag=ORGANIZATION] 
    //
    //
    try {
        String[] splitArray = stringOutput.split("\\s+");
        
        String cleanOutput = "";
         
         for (int i=0; i<splitArray.length; i++){
             
             //System.out.println(splitArray[i]);
             if( splitArray[i].contains("Text") ){
                 cleanOutput += "\r\n";
                 cleanOutput += splitArray[i];
                
             }
             if( splitArray[i].contains("NamedEntityTag") ){
                 cleanOutput += "\r\n";
                 cleanOutput += splitArray[i]; 
                 cleanOutput += " \r\n";
             }
        }
         //send to clean output text file
         out.println(cleanOutput);
    } catch (PatternSyntaxException ex) {}
    
   //System.out.println(stringOutput);
       
  }

}

